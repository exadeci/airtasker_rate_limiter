require 'rails_helper'

RSpec.describe HomeController do

  context 'when loading the home index' do
    describe 'GET #index' do
      subject { get :index}

      it 'returns ok' do
        subject
        expect(response.content_type).to eq('text/plain')
        expect(response.body).to eq('ok')
      end
    end
  end
end
