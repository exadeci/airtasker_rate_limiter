require 'rails_helper'

RSpec.describe RateLimiter, type: :request do

  context 'when the client does only one request' do
    it 'returns ok' do
      get '/home'
      expect(response.body).to eq('ok')
    end
  end

  context 'when the client does more than 100 requests' do
    it 'returns 429 and a message' do
      120.times do
        get '/home'
      end
      expect(response.code).to eq('429')
      expect(response.body).to start_with('Rate limit exceeded. Try again in')
    end
  end
end
