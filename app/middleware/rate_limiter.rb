class RateLimiter
  def initialize(app, options = {})
    @app = app
    @options = options
  end

  def call(env)
    request = ActionDispatch::Request.new(env)
    requester_ip = request.remote_ip
    requester_key = "requests:#{requester_ip}"
    if allowed?(requester_key)
      @app.call(env)
    else
      time = $redis.ttl(requester_key)
      rate_limit_exceeded(request, time)
    end
  end

  def allowed?(requester_key)
    if RedisInterface.count_in_redis(requester_key).to_i <= Rails.application.secrets.rate_limiter[:max_requests]
      true
    else
      false
    end
  end

  def rate_limit_exceeded(request, time)
    headers = respond_to?(:retry_after) ? {'Retry-After' => retry_after.to_f.ceil.to_s} : {}
    content_type = 'text/plain; charset=utf-8'
    [429, {'Content-Type' => content_type}.merge(headers), ["Rate limit exceeded. Try again in #{time}"]]
  end
end
