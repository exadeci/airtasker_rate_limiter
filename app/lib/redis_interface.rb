module RedisInterface
  extend self

  def count_in_redis(requester_key)
    count = $redis.get(requester_key)
    if count.nil?
      initialize_in_redis(requester_key)
      return 1
    else
      increment_in_redis(requester_key)
    end
    count
  end

  def initialize_in_redis(requester_key)
    $redis.set(requester_key, 0)
    $redis.expire(requester_key, Rails.application.secrets.rate_limiter[:timeout])
  end

  def increment_in_redis(requester_key)
    $redis.incr(requester_key)
  end

end
