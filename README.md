# Rate Limiter poc

This project is the proof of concept of a rate limiter managed by a middleware.

The middleware is located in `app/middleware/rate_limiter.rb`.

It limits the number of requests that can be done on the server to **100/hour/client**.

Installation
============
A redis server is required, the project uses redis default configuration.

```
git clone https://exadeci@bitbucket.org/exadeci/airtasker_rate_limiter.git
cd airtasker_rate_limiter
bundle install
```

Running
=======
`bundle exec rails server -p 3000`

Testing
=======
`bundle exec rspec`
