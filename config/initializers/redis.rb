$redis = ConnectionPool::Wrapper.new(size: Rails.application.secrets.redis[:pool_size],
                            timemout: Rails.application.secrets.redis[:timeout]) do
  Redis.new({url: Rails.application.secrets.redis[:url]})
end
